using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float _torqueValue = 10f;
    private float _normalSpeed = 20f;
    private float _boostSpeed = 40f;

    private Rigidbody2D _rigidbody2D;
    private SurfaceEffector2D _surfaceEffector2D;

    private bool _canMove = true;

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _surfaceEffector2D = FindObjectOfType<SurfaceEffector2D>();
    }

    private void Update()
    {
        if (_canMove)
        {
            FlipPlayer();
            Boost();
        }
    }

    private void Boost()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _surfaceEffector2D.speed = _boostSpeed;
        }
        else
        {
            _surfaceEffector2D.speed = _normalSpeed;
        }
    }

    private void FlipPlayer()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _rigidbody2D.AddTorque(_torqueValue);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _rigidbody2D.AddTorque(-_torqueValue);
        }
    }

    public void DisableControls()
    {
        _canMove = false;
    }
}

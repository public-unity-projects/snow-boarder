using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowCollision : MonoBehaviour
{

    [SerializeField] ParticleSystem _snowEffect;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground") && !_snowEffect.isPlaying)
        {
            _snowEffect.Play();
        }
    }

    private void OnCollisionExit2D()
    {
        if (_snowEffect.isPlaying)
        {
            _snowEffect.Stop();
        }
    }

}

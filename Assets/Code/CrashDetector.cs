using UnityEngine;
using UnityEngine.SceneManagement;

public class CrashDetector : MonoBehaviour
{
    [SerializeField] AudioClip _crashClip;
    [SerializeField] ParticleSystem _crashEffect;
    private float _crashDelay = .5f;

    private bool _hasCrashed = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground") && !_hasCrashed)
        {
            _hasCrashed = true;

            FindObjectOfType<PlayerController>().DisableControls();

            AudioSource.PlayClipAtPoint(_crashClip, Camera.main.transform.position);

            _crashEffect.Play();
            Invoke("ReloadScene", _crashDelay);
        }
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }

}

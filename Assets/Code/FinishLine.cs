using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine : MonoBehaviour
{
    [SerializeField] AudioClip _finishClip;
    [SerializeField] ParticleSystem _finishEffect;
    private float _winDelay = 1f;

    private bool _hasFinished = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && !_hasFinished)
        {
            _hasFinished = true;

            AudioSource.PlayClipAtPoint(_finishClip, Camera.main.transform.position);

            _finishEffect.Play();
            Invoke("ReloadScene", _winDelay);
        }
    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
